/**
 * ProductsImplementsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package WS;

public interface ProductsImplementsService extends javax.xml.rpc.Service {
    public java.lang.String getProductsImplementsPortAddress();

    public WS.IProduct getProductsImplementsPort() throws javax.xml.rpc.ServiceException;

    public WS.IProduct getProductsImplementsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
