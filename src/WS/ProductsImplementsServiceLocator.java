/**
 * ProductsImplementsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package WS;

public class ProductsImplementsServiceLocator extends org.apache.axis.client.Service implements WS.ProductsImplementsService {

    public ProductsImplementsServiceLocator() {
    }


    public ProductsImplementsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ProductsImplementsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ProductsImplementsPort
    private java.lang.String ProductsImplementsPort_address = "http://127.0.0.1:4800/ws/products";

    public java.lang.String getProductsImplementsPortAddress() {
        return ProductsImplementsPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProductsImplementsPortWSDDServiceName = "ProductsImplementsPort";

    public java.lang.String getProductsImplementsPortWSDDServiceName() {
        return ProductsImplementsPortWSDDServiceName;
    }

    public void setProductsImplementsPortWSDDServiceName(java.lang.String name) {
        ProductsImplementsPortWSDDServiceName = name;
    }

    public WS.IProduct getProductsImplementsPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProductsImplementsPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProductsImplementsPort(endpoint);
    }

    public WS.IProduct getProductsImplementsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            WS.ProductsImplementsPortBindingStub _stub = new WS.ProductsImplementsPortBindingStub(portAddress, this);
            _stub.setPortName(getProductsImplementsPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProductsImplementsPortEndpointAddress(java.lang.String address) {
        ProductsImplementsPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (WS.IProduct.class.isAssignableFrom(serviceEndpointInterface)) {
                WS.ProductsImplementsPortBindingStub _stub = new WS.ProductsImplementsPortBindingStub(new java.net.URL(ProductsImplementsPort_address), this);
                _stub.setPortName(getProductsImplementsPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ProductsImplementsPort".equals(inputPortName)) {
            return getProductsImplementsPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://WS/", "ProductsImplementsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://WS/", "ProductsImplementsPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ProductsImplementsPort".equals(portName)) {
            setProductsImplementsPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
