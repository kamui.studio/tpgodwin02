package WS;

public class IProductProxy implements WS.IProduct {
  private String _endpoint = null;
  private WS.IProduct iProduct = null;
  
  public IProductProxy() {
    _initIProductProxy();
  }
  
  public IProductProxy(String endpoint) {
    _endpoint = endpoint;
    _initIProductProxy();
  }
  
  private void _initIProductProxy() {
    try {
      iProduct = (new WS.ProductsImplementsServiceLocator()).getProductsImplementsPort();
      if (iProduct != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iProduct)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iProduct)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iProduct != null)
      ((javax.xml.rpc.Stub)iProduct)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public WS.IProduct getIProduct() {
    if (iProduct == null)
      _initIProductProxy();
    return iProduct;
  }
  
  public WS.Products findById(int arg0) throws java.rmi.RemoteException{
    if (iProduct == null)
      _initIProductProxy();
    return iProduct.findById(arg0);
  }
  
  public WS.Products[] findAll() throws java.rmi.RemoteException{
    if (iProduct == null)
      _initIProductProxy();
    return iProduct.findAll();
  }
  
  
}