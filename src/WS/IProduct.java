/**
 * IProduct.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package WS;

public interface IProduct extends java.rmi.Remote {
    public WS.Products findById(int arg0) throws java.rmi.RemoteException;
    public WS.Products[] findAll() throws java.rmi.RemoteException;
}
