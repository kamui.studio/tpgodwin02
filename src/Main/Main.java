package Main;

import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;

import WS.IProduct;
import WS.Products;
import WS.ProductsImplementsService;
import WS.ProductsImplementsServiceLocator;

public class Main {

	public static void main(String[] args) {

		try {
			
			ProductsImplementsService ProductsImplementsService = new ProductsImplementsServiceLocator();
			IProduct product = ProductsImplementsService.getProductsImplementsPort();
			System.out.println("First product id = " + product.findById(2).getUid() + " and name = " + product.findById(2).getName());
		
			
			for (Products p : product.findAll()) {
				System.out.println(p.getUid() + " - " + p.getName() + " : " + p.getPrice() + "$\r\n" + p.getDesc() + "");
			}

			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
	}

}
